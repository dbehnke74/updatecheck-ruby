require "spec_helper"

RSpec.describe Updatecheck do
  it "has Software and default methods" do
    software = Updatecheck::Software.new("nothing")
    expect(software).to respond_to(:current_version)
    expect(software).to respond_to(:download_url)
    expect(software).to respond_to(:refresh)
  end

  it "can load plugins" do
    expect(Updatecheck.load_plugins).to eq(true)
  end
end

Updatecheck.load_plugins

RSpec.describe "FirefoxESR" do
  it "exists" do
    software = Updatecheck::FirefoxESR.new("nothing")
  end
  
  it "has required methods" do
    software = Updatecheck::FirefoxESR.new("nothing")
    expect(software).to respond_to(:current_version)
    expect(software).to respond_to(:download_url)
    expect(software).to respond_to(:refresh)
  end

  it "supports mac" do
    software = Updatecheck::FirefoxESR.new("mac")
    expect(software.current_version).to match /^\d{1,}\.\d{1,}\.\d{1,}$/
    expect(software.download_url).to match /^.{1,}\.dmg$/
  end

  it "supports windows" do
    software = Updatecheck::FirefoxESR.new("win")
    expect(software.current_version).to match /^\d{1,}\.\d{1,}\.\d{1,}$/
    expect(software.download_url).to match /^.{1,}\.exe$/
  end
end


RSpec.describe "VirtualBox" do
  it "exists" do
    software = Updatecheck::VirtualBox.new("nothing")
  end
  
  it "has required methods" do
    software = Updatecheck::VirtualBox.new("nothing")
    expect(software).to respond_to(:current_version)
    expect(software).to respond_to(:download_url)
    expect(software).to respond_to(:refresh)
  end

  it "supports mac" do
    software = Updatecheck::VirtualBox.new("mac")
    expect(software.current_version).to match /^\d{1,}\.\d{1,}\.\d{1,}$/
    expect(software.download_url).to match /^.{1,}\.dmg$/
  end

  it "supports windows" do
    software = Updatecheck::VirtualBox.new("win")
    expect(software.current_version).to match /^\d{1,}\.\d{1,}\.\d{1,}$/
    expect(software.download_url).to match /^.{1,}\.exe$/
  end
end

RSpec.describe "OracleJavaJDK" do
  it "exists" do
    software = Updatecheck::OracleJavaJDK.new("nothing")
  end
  
  it "has required methods" do
    software = Updatecheck::OracleJavaJDK.new("nothing")
    expect(software).to respond_to(:current_version)
    expect(software).to respond_to(:download_url)
    expect(software).to respond_to(:refresh)
  end

  it "supports mac" do
    software = Updatecheck::OracleJavaJDK.new("mac")
    expect(software.current_version).to match /^\du\d{1,3}-b\d{1,3}$/
    expect(software.download_url).to match /^htt.{1,}\.dmg$/
  end

  it "supports windows" do
    software = Updatecheck::OracleJavaJDK.new("win")
    expect(software.current_version).to match /^\du\d{1,3}-b\d{1,3}$/
    expect(software.download_url).to match /^htt.{1,}\.exe$/
  end

  it "supports linux" do
    software = Updatecheck::OracleJavaJDK.new("linux")
    expect(software.current_version).to match /^\du\d{1,3}-b\d{1,3}$/
    expect(software.download_url).to match /^htt.{1,}\.tar\.gz$/
  end
end
