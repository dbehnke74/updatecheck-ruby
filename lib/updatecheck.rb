require "updatecheck/version"

module Updatecheck
  class Software
    def initialize(os)
      @name = "Software"
      @os = os
      @cache = { 'download_url' => "unknown", 'current_version' => "unknown" }
    end
 
    def refresh
      # obtain the download url
      # obtain the current version
      true
    end

    def current_version
      refresh if @cache['current_version'] == "unknown"
      @cache['current_version']
    end
  
    def download_url
      refresh if @cache['download_url'] == "unknown"
      @cache['download_url']
    end
  end

  def self.load_plugins
    Dir.glob(File.dirname(__FILE__) + '/plugins/*.rb') do |f|
      Updatecheck.class_eval File.read(f)
    end
    true
  end

  def self.check_software(c)
    o = Object.const_get('Updatecheck').const_get(c.to_s)
    software = { 'mac' => o.new('mac'),
                 'win' => o.new('win'),
                 'linux' => o.new('linux')
               }
    ['mac','win'].each do |s|
      puts "#{software[s].name} #{software[s].current_version} #{software[s].download_url}"
    end
  end
  
  def self.demo
    Updatecheck.load_plugins
    Updatecheck.constants.each do |c|
      if Updatecheck.const_get(c).is_a? Class
        if c.to_s != "Software"
          check_software(c)
        end
      end
    end
  end
end
