require 'httparty'
require 'nokogiri'

class VirtualBox < Software
  def initialize(os)
    super(os)
    @name = "VirtualBox"
  end
 
  def refresh
    # obtain the downloadurl
    response = HTTParty.get('https://www.virtualbox.org/wiki/Downloads', follow_redirects: false)
    if response.code != 200
      return false
    end

    x = Nokogiri::HTML(response.body)
    x.xpath("//a").each do |a|
      href = a['href']
      if href.match('OSX.dmg') && @os == "mac"
        @cache['download_url'] = href
        break
      end
      if href.match('Win.exe') && @os == "win"
        @cache['download_url'] = href
        break
      end 
    end

    # obtain the version
    # use the download_url to determine

    du = @cache['download_url']
    return false if du == "unknown"
    a = download_url.split('/')
    if a.length > 4
      @cache['current_version'] = a[4]
      return true
    end
    false
  end
end

