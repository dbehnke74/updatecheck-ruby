require 'httparty'

class FirefoxESR < Software
  def initialize(os)
    super(os)
    @name = "FirefoxESR"
  end
 
  def refresh
    #obtain the download url
    url = { 
      'mac' => 'https://download.mozilla.org/?product=firefox-esr-latest&os=osx&lang=en-US',
      'win' => 'https://download.mozilla.org/?product=firefox-esr-latest&os=win&lang=en-US'
    }

    response = HTTParty.get(url[@os], follow_redirects: false)
    return false if response.code != 302
      
    @cache['download_url'] = response.headers['location']

    #obtain the current version
    a = @cache['download_url'].split('/')
    if a.length > 6
      b = a[6]
      @cache['current_version'] = b.split('esr')[0]
      return true
    end
    false
  end
end

