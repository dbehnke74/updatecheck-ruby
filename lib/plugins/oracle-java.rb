require 'httparty'
require 'nokogiri'

class OracleJavaJDK < Software
  def initialize(os)
    super(os)
    @name = "Oracle Java JDK"
  end
 
  def refresh
    # obtain the downloadurl
    # note that won't be able to download this without some cookie tricks
    # reference: https://gist.github.com/P7h/9741922

    # example urls
    # mac: http://download.oracle.com/otn-pub/java/jdk/8u144-b01/090f390dda5b47b9b721c7dfaa008135/jdk-8u144-macosx-x64.dmg
    # windows: http://download.oracle.com/otn-pub/java/jdk/8u144-b01/090f390dda5b47b9b721c7dfaa008135/jdk-8u144-windows-x64.exe
    # linux: http://download.oracle.com/otn-pub/java/jdk/8u144-b01/090f390dda5b47b9b721c7dfaa008135/jdk-8u144-linux-x64.tar.gz

    matchstring = {
      'mac' => /http:\/\/download.oracle.com\/otn-pub\/java\/jdk.{1,}macosx-x64.dmg/,
      'win' => /http:\/\/download.oracle.com\/otn-pub\/java\/jdk.{1,}windows-x64.exe/,
      'linux' => /http:\/\/download.oracle.com\/otn-pub\/java\/jdk.{1,}linux-x64.tar.gz/
    }

    response = HTTParty.get('http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html', follow_redirects: false)
    if response.code != 200
      return false
    end
    body = response.body
    du = body.match matchstring[@os]
    return false if du == nil
    @cache['download_url'] = du[0]

    # obtain the version
    # use the download_url to determine

    cv = du[0].match /\du\d{1,3}-b\d{1,3}/
    return false if cv == nil
    @cache['current_version'] = cv[0]

    true
  end
end

